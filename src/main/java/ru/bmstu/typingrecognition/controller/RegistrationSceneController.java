package ru.bmstu.typingrecognition.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import ru.bmstu.typingrecognition.entity.KeyEventInfo;
import ru.bmstu.typingrecognition.math.InputVectorComputer;
import ru.bmstu.typingrecognition.math.MathModule;

import java.io.*;
import java.net.URL;
import java.util.*;

import static ru.bmstu.typingrecognition.controller.constant.SceneControllersConstants.*;
import static ru.bmstu.typingrecognition.controller.util.SceneControllersUtils.*;

public class RegistrationSceneController implements Initializable {

    @FXML private Label loginLabel;
    @FXML private Label passwordLabel;
    @FXML private Label remainingTimesLabel;
    @FXML private TextField loginField;
    @FXML private PasswordField passwordField;
    @FXML private Button nextButton;
    @FXML private Button cancelButton;

    private InputVectorComputer inputVectorComputer;

    private List<KeyEventInfo> keyEventList;
    private Map<String, KeyEventInfo> lastPressedKeys;

    private String login;
    private String password;
    private int currentRemainingTimes;
    private double[][] inputVectors;
    private boolean isDeleteOrBackspacePressed;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        loginLabel.setText("Имя пользователя");
        passwordLabel.setText("Пароль");
        remainingTimesLabel.setText(String.valueOf(PASSWORD_ENTRIES_NUMBER));

        loginField.setPromptText("Введите имя пользователя");

        passwordField.setPromptText("Введите пароль");
        passwordField.addEventHandler(KeyEvent.KEY_PRESSED, this::onKeyPressed);
        passwordField.addEventHandler(KeyEvent.KEY_RELEASED, this::onKeyReleased);

        nextButton.setText("Далее");
        nextButton.setOnAction(this::onNextButtonAction);

        cancelButton.setText("Отмена");
        cancelButton.setOnAction(this::onCancelButtonAction);

        inputVectorComputer = new InputVectorComputer();

        keyEventList = Collections.synchronizedList(new ArrayList<>());
        lastPressedKeys = Collections.synchronizedMap(new HashMap<>());
        login = null;
        password = null;
        currentRemainingTimes = PASSWORD_ENTRIES_NUMBER;
        inputVectors = null;
        isDeleteOrBackspacePressed = false;
    }

    private void onKeyPressed(KeyEvent event) {
        if (NotServiceKey(event)) {
            KeyEventInfo eventInfo = new KeyEventInfo(event.getText(), System.currentTimeMillis());
            keyEventList.add(eventInfo);
            lastPressedKeys.put(eventInfo.getKey(), eventInfo);
        } else if (isDeleteOrBackspace(event))  {
            isDeleteOrBackspacePressed = true;
        }
    }

    private void onKeyReleased(KeyEvent event) {
        if (NotServiceKey(event)) {
            String character = event.getText();
            KeyEventInfo eventInfo = Optional.ofNullable(lastPressedKeys.get(character))
                    .orElse(lastPressedKeys.get(character.toUpperCase()));
            eventInfo.setUpTime(System.currentTimeMillis());
        } else if (isDeleteOrBackspace(event))  {
            isDeleteOrBackspacePressed = true;
        }
    }

    private void onNextButtonAction(ActionEvent event) {
        if (isDeleteOrBackspacePressed) {
            System.out.println("Delete or Backspace key was pressed when entering the password!\n");
            openModalWindow((Stage) nextButton.getScene().getWindow(),
                    "При вводе пароля было\nзафиксировано исправление!\nПопытка не засчитана.");
            clearPasswordEntryData();
            return;
        }
        if (password == null) {
            String trimmedLogin = loginField.getText().trim();
            if (trimmedLogin.isEmpty()) {
                System.out.println("Login field is empty!\n");
                openModalWindow((Stage) nextButton.getScene().getWindow(), "Имя пользователя не заполнено!");
                loginField.clear();
                clearPasswordEntryData();
                return;
            }

            File userReferenceFile = new File(PATH_TO_REFERENCES_DIR + "/" + login);
            if (userReferenceFile.exists()) {
                System.out.println("User is already registered!\n");
                openModalWindow((Stage) nextButton.getScene().getWindow(),
                        "Такой пользователь\nуже зарегистрирован!");
                cleanUpWindow();
                return;
            }

            if (keyEventList.size() > MIN_CHARS_NUMBER) {
                login = trimmedLogin;
                password = passwordField.getText();
                inputVectors = new double[PASSWORD_ENTRIES_NUMBER][keyEventList.size()];
                loginField.setEditable(false);
                recordPasswordEntry();
            } else {
                System.out.println("Password characters number must be more than " +
                        MIN_CHARS_NUMBER + "!\n");
                openModalWindow((Stage) nextButton.getScene().getWindow(),
                        "Число символов в пароле\nдолжно быть больше " + MIN_CHARS_NUMBER + "!");
                clearPasswordEntryData();
            }
        } else {
            if (passwordField.getText().equals(password)) {
                recordPasswordEntry();
                if (currentRemainingTimes == 0) {
                    computeAndSaveUserReference();
                    cancelButton.fire();
                }
            } else {
                System.out.println("Entered password is not match previous ones!\n");
                openModalWindow((Stage) nextButton.getScene().getWindow(),
                        "Введенный пароль не совпадает\nс предыдущими!");
                clearPasswordEntryData();
            }
        }
    }

    private void onCancelButtonAction(ActionEvent event) {
        cleanUpWindow();
        closeCurrentAndOpenNextWindow((Stage) cancelButton.getScene().getWindow(),
                PATH_TO_LOGIN_SCENE_FXML_FILE, "Вход в систему");
    }

    private void recordPasswordEntry() {
        double[] inputVector = inputVectorComputer.compute(keyEventList);
        inputVectors[PASSWORD_ENTRIES_NUMBER - currentRemainingTimes] = inputVector;
        remainingTimesLabel.setText(String.valueOf(--currentRemainingTimes));
        clearPasswordEntryData();
    }

    private void clearPasswordEntryData() {
        keyEventList.clear();
        lastPressedKeys.clear();
        passwordField.clear();
        isDeleteOrBackspacePressed = false;
    }

    private void computeAndSaveUserReference() {
        File referencesDir = new File(PATH_TO_REFERENCES_DIR);
        if (!referencesDir.exists() && !referencesDir.mkdir()) {
            System.out.println("Error creating directory!\n");
            return;
        }
        try(FileWriter writer = new FileWriter(PATH_TO_REFERENCES_DIR + "/" + login)) {
            MathModule mathModule = new MathModule(inputVectors);
            double[] expectedValues = mathModule.getExpectedValues();
            double[][] invCovMatrix = mathModule.getInverseCovarianceMatrix();
            writer.write(password + "\n");
            writer.write(doubleArrayToStringLine(expectedValues));
            for (double[] row : invCovMatrix) {
                writer.write(doubleArrayToStringLine(row));
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error writing file!\n");
        }
    }

    private void cleanUpWindow() {
        loginField.setEditable(true);
        loginField.clear();
        clearPasswordEntryData();
        login = null;
        password = null;
        currentRemainingTimes = PASSWORD_ENTRIES_NUMBER;
        remainingTimesLabel.setText(String.valueOf(PASSWORD_ENTRIES_NUMBER));
        inputVectors = null;
        isDeleteOrBackspacePressed = false;
    }
}

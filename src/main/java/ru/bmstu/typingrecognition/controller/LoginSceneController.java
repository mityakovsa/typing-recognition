package ru.bmstu.typingrecognition.controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import ru.bmstu.typingrecognition.entity.KeyEventInfo;
import ru.bmstu.typingrecognition.math.InputVectorComputer;
import ru.bmstu.typingrecognition.math.MathModule;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.util.*;

import static ru.bmstu.typingrecognition.controller.constant.SceneControllersConstants.*;
import static ru.bmstu.typingrecognition.controller.util.SceneControllersUtils.*;

public class LoginSceneController implements Initializable {

    @FXML private Label loginLabel;
    @FXML private Label passwordLabel;
    @FXML private TextField loginField;
    @FXML private PasswordField passwordField;
    @FXML private Button okButton;
    @FXML private Button cancelButton;
    @FXML private Button registrationButton;

    private List<KeyEventInfo> keyEventList;
    private Map<String, KeyEventInfo> lastPressedKeys;

    private String loadedPassword;
    private double[] loadedExpectedValues;
    private double[][] loadedInvCovMatrix;
    private boolean isDeleteOrBackspacePressed;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        loginLabel.setText("Имя пользователя");

        passwordLabel.setText("Пароль");

        loginField.setPromptText("Введите имя пользователя");

        passwordField.setPromptText("Введите пароль");
        passwordField.addEventHandler(KeyEvent.KEY_PRESSED, this::onKeyPressed);
        passwordField.addEventHandler(KeyEvent.KEY_RELEASED, this::onKeyReleased);

        okButton.setText("ОК");
        okButton.setOnAction(this::onOkButtonAction);

        cancelButton.setText("Отмена");
        cancelButton.setOnAction(event -> Platform.exit());

        registrationButton.setText("Регистрация");
        registrationButton.setOnAction(this::onRegistrationButtonAction);

        keyEventList = Collections.synchronizedList(new ArrayList<>());
        lastPressedKeys = Collections.synchronizedMap(new HashMap<>());

        loadedPassword = null;
        loadedExpectedValues = null;
        loadedInvCovMatrix = null;
        isDeleteOrBackspacePressed = false;
    }

    private void onKeyPressed(KeyEvent event) {
        if (NotServiceKey(event)) {
            KeyEventInfo eventInfo = new KeyEventInfo(event.getText(), System.currentTimeMillis());
            keyEventList.add(eventInfo);
            lastPressedKeys.put(eventInfo.getKey(), eventInfo);
        } else if (isDeleteOrBackspace(event)) {
            isDeleteOrBackspacePressed = true;
        }
    }

    private void onKeyReleased(KeyEvent event) {
        if (NotServiceKey(event)) {
            String character = event.getText();
            KeyEventInfo eventInfo = Optional.ofNullable(lastPressedKeys.get(character))
                    .orElse(lastPressedKeys.get(character.toUpperCase()));
            eventInfo.setUpTime(System.currentTimeMillis());
        } else if (isDeleteOrBackspace(event)) {
            isDeleteOrBackspacePressed = true;
        }
    }

    private void onOkButtonAction(ActionEvent event) {
        if (isDeleteOrBackspacePressed) {
            System.out.println("Delete or Backspace key was pressed when entering the password!\n");
            openModalWindow((Stage) okButton.getScene().getWindow(),
                    "При вводе пароля было\nзафиксировано исправление!\nПопытка не засчитана.");
            cleanUpWindow();
            return;
        }
        final String login = loginField.getText().trim();
        final String password = passwordField.getText();
        if (login.isEmpty() || password.isEmpty()) {
            System.out.println("Login or password field is empty!\n");
            openModalWindow((Stage) okButton.getScene().getWindow(),
                    "Имя пользователя или пароль\nне заполнены!");
            cleanUpWindow();
            return;
        }
        File userReferenceFile = new File(PATH_TO_REFERENCES_DIR + "/" + login);
        if (!userReferenceFile.exists()) {
            System.out.println("User is not registered!\n");
            openModalWindow((Stage) okButton.getScene().getWindow(), "Такой пользователь\nне зарегистрирован!");
            cleanUpWindow();
            return;
        }
        try {
            loadUserReference(userReferenceFile);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error reading file!\n");
            cleanUpWindow();
            return;
        }
        if (!password.equals(loadedPassword)) {
            System.out.println("Wrong password!\n");
            openModalWindow((Stage) okButton.getScene().getWindow(), "Неверный пароль!");
            cleanUpWindow();
            return;
        }
        InputVectorComputer generator = new InputVectorComputer();
        double[] inputVector = generator.compute(keyEventList);
        MathModule mathModule = new MathModule(loadedInvCovMatrix, loadedExpectedValues,
                inputVector, STUDENTS_COEFFICIENT);
        double g = mathModule.getDiscriminantFunctionValue();
        if (g < 0) {
            System.out.println("Access granted");
            openModalWindow((Stage) okButton.getScene().getWindow(), "Доступ разрешен");
        } else {
            System.out.println("Access denied");
            openModalWindow((Stage) okButton.getScene().getWindow(), "Доступ запрещен");
        }
        cleanUpWindow();
    }

    private void onRegistrationButtonAction(ActionEvent event) {
        cleanUpWindow();
        closeCurrentAndOpenNextWindow((Stage) registrationButton.getScene().getWindow(),
                PATH_TO_REGISTRATION_SCENE_FXML_FILE, "Регистрация");
    }

    private void cleanUpWindow() {
        loadedPassword = null;
        loadedExpectedValues = null;
        loadedInvCovMatrix = null;
        isDeleteOrBackspacePressed = false;
        keyEventList.clear();
        lastPressedKeys.clear();
        loginField.clear();
        passwordField.clear();
    }

    private void loadUserReference(File userReferenceFile) throws IOException {
        List<String> content = Files.readAllLines(userReferenceFile.toPath());
        if (content.isEmpty())
            throw new IOException("File " + userReferenceFile.getPath() + " is empty");
        loadedPassword = content.get(0);
        loadedExpectedValues = stringLineToDoubleArray(content.get(1));
        int n = loadedExpectedValues.length;
        loadedInvCovMatrix = new double[n][];
        for (int i = 0; i < n; i++) {
            loadedInvCovMatrix[i] = stringLineToDoubleArray(content.get(i + 2));
        }
    }
}

package ru.bmstu.typingrecognition.controller.constant;

public class SceneControllersConstants {

    public static final int MIN_CHARS_NUMBER = 10;
    public static final int PASSWORD_ENTRIES_NUMBER = 40;
    public static final String PATH_TO_REFERENCES_DIR = "./references";
//    public static final double STUDENTS_COEFFICIENT = 3.69;
    public static final double STUDENTS_COEFFICIENT = 12.71;
    public static final String PATH_TO_LOGIN_SCENE_FXML_FILE = "/fxml/login-scene.fxml";
    public static final String PATH_TO_REGISTRATION_SCENE_FXML_FILE = "/fxml/registration-scene.fxml";
}

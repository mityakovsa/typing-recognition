package ru.bmstu.typingrecognition.controller.util;

import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import ru.bmstu.typingrecognition.MainApp;

import java.io.IOException;
import java.util.Arrays;

public class SceneControllersUtils {

    public static boolean NotServiceKey(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        boolean isServiceKey = false;
        switch (keyCode) {
            case SHIFT:
            case CONTROL:
            case ALT:
            case CAPS:
            case TAB:
            case ENTER:
            case BACK_SPACE:
            case DELETE:
            case HOME:
            case END:
            case RIGHT:
            case LEFT:
                isServiceKey = true;
        }
        return !isServiceKey;
    }

    public static boolean isDeleteOrBackspace(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        switch (keyCode) {
            case BACK_SPACE:
            case DELETE:
                return true;
        }
        return false;
    }

    public static String doubleArrayToStringLine(double[] doubles) {
        StringBuilder stringBuilder = new StringBuilder();
        for (double element : doubles) {
            stringBuilder.append(element);
            stringBuilder.append(" ");
        }
        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        stringBuilder.append("\n");
        return stringBuilder.toString();
    }

    public static double[] stringLineToDoubleArray(String line) {
        return Arrays.stream(line.split(" "))
                .mapToDouble(Double::parseDouble)
                .toArray();
    }

    public static void closeCurrentAndOpenNextWindow(Stage currentStage, String pathToNextFxml, String title) {
        try {
            Parent parent = FXMLLoader.load(MainApp.class.getResource(pathToNextFxml));
            Stage nextStage = new Stage();
            nextStage.setTitle(title);
            nextStage.setScene(new Scene(parent));
            currentStage.close();
            nextStage.show();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Window opening error!\n");
        }
    }

    public static void openModalWindow(Stage owner, String message) {
        Stage window = new Stage(StageStyle.UNDECORATED);
        window.initModality(Modality.WINDOW_MODAL);
        window.initOwner(owner);
        BorderPane pane = new BorderPane();
        Label lbl = new Label(message);
        Button btn = new Button("ОК");
        btn.setPrefHeight(30.0);
        btn.setPrefWidth(100.0);
        pane.setCenter(lbl);
        pane.setBottom(btn);
        BorderPane.setAlignment(btn, Pos.CENTER);
        pane.setPadding(new Insets(10.0));
        btn.setOnAction(event -> window.close());
        window.setScene(new Scene(pane, 250, 120));
        window.show();
    }
}

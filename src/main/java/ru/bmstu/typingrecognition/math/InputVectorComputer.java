package ru.bmstu.typingrecognition.math;

import ru.bmstu.typingrecognition.entity.KeyEventInfo;

import java.util.Arrays;
import java.util.List;

public class InputVectorComputer {

    public static final int S_MAX = 900;

    public double[] compute(List<KeyEventInfo> keyEventList) {

        int n = keyEventList.size();

        double[] pressVector = new double[n];
        long pressSum = 0L;
        long maxPress = 0L;

        double[] pauseVector = new double[n - 1];
        long pauseSum = 0L;
        long maxPause = 0L;

        for (int i = 0; i < n - 1; i++) {

            long pressTime = keyEventList.get(i).getUpTime() - keyEventList.get(i).getDownTime();
            pressVector[i] = (double) pressTime;
            pressSum += pressTime;
            maxPress = Math.max(pressTime, maxPress);

            long pauseTime = keyEventList.get(i + 1).getDownTime() - keyEventList.get(i).getUpTime();
            pauseVector[i] = (double) pauseTime;
            pauseSum += pauseTime;
            maxPause = Math.max(pauseTime, maxPause);
        }

        long lastPressTime = keyEventList.get(n - 1).getUpTime() - keyEventList.get(n - 1).getDownTime();
        pressVector[n - 1] = (double) lastPressTime;
        pressSum += lastPressTime;
        maxPress = Math.max(lastPressTime, maxPress);

        double expectedPress = (double) pressSum / n;
        double finalMaxPress = maxPress;
        double beta = Math.sqrt(
                Arrays.stream(pressVector)
                        .map(pressTime -> Math.pow(pressTime / finalMaxPress - expectedPress, 2))
                        .sum()
                        / (n - 1)
        );

        double s = (keyEventList.get(n - 1).getUpTime() - keyEventList.get(0).getDownTime()) * S_MAX / 60.0;

        double expectedPause = (double) pauseSum / (maxPause * (n - 1));
        double finalMaxPause = maxPause;
        double alpha = Math.sqrt(
                Arrays.stream(pauseVector)
                        .map(pauseTime -> Math.pow(pauseTime / finalMaxPause - expectedPause, 2))
                        .sum()
                        / (n - 2)
        );

        double[] inputVector = new double[2 * n + 4];
        System.arraycopy(pressVector, 0, inputVector, 0, pressVector.length);
        System.arraycopy(pauseVector, 0, inputVector, n, pauseVector.length);
        inputVector[2 * n - 1] = expectedPause;
        inputVector[2 * n] = alpha;
        inputVector[2 * n + 1] = expectedPress;
        inputVector[2 * n + 2] = beta;
        inputVector[2 * n + 3] = s;

        return inputVector;
    }
}

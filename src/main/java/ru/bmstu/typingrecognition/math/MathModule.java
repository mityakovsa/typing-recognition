package ru.bmstu.typingrecognition.math;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.MatrixUtils;

public class MathModule {

    private double[][] inputVectors;
    private int vectorsNumber;
    private final int characteristicsNumber;

    private double[][] invCovMatrix;
    private double[] expectedValues;
    private double[] inputVector;
    private double studentsCoeff;

    public MathModule(double[][] inputVectors) {
        this.inputVectors = inputVectors;
        vectorsNumber = inputVectors.length;
        characteristicsNumber = inputVectors[0].length;
        this.expectedValues = null;
    }

    public MathModule(double[][] invCovMatrix, double[] expectedValues, double[] inputVector, double studentsCoeff) {
        this.invCovMatrix = invCovMatrix;
        this.expectedValues = expectedValues;
        this.inputVector = inputVector;
        this.characteristicsNumber = inputVector.length;
        this.studentsCoeff = studentsCoeff;
    }

    public double[][] getInverseCovarianceMatrix() {
        double[][] covMatrix = new double[characteristicsNumber][characteristicsNumber];
        getExpectedValues();
        for (int i = 0; i < characteristicsNumber; i++) {
            for (int j = 0; j < characteristicsNumber; j++) {
                covMatrix[i][j] = getCovarianceMatrixElementNumerator(i, j) / vectorsNumber;
            }
        }
        return MatrixUtils.inverse(new Array2DRowRealMatrix(covMatrix))
                .getData();
    }

    public double[] getExpectedValues() {
        if (expectedValues == null) {
            expectedValues = new double[characteristicsNumber];
            for (int j = 0; j < characteristicsNumber; j++) {
                expectedValues[j] = 0.0;
                for (double[] inputVector : inputVectors) {
                    expectedValues[j] += inputVector[j];
                }
                expectedValues[j] = expectedValues[j] / vectorsNumber;
            }
        }
        return expectedValues;
    }

    public double getDiscriminantFunctionValue() {
        double g = 0.0;
        for (int i = 0; i < characteristicsNumber; i++) {
            for (int j = 0; j < characteristicsNumber; j++) {
                g += invCovMatrix[i][j] * (inputVector[i] - expectedValues[i]) * (inputVector[j] - expectedValues[j]);
            }
        }
        return 0.5 * g - Math.pow(studentsCoeff, 2);
    }

    private double getCovarianceMatrixElementNumerator(int i, int j) {
        double sum = 0.0;
        for (double[] inputVector : inputVectors) {
            sum += (inputVector[i] - expectedValues[i]) * (inputVector[j] - expectedValues[j]);
        }
        return sum;
    }
}

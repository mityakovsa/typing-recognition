package ru.bmstu.typingrecognition.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KeyEventInfo {
    private String key;
    private Long downTime;
    private Long upTime;

    public KeyEventInfo(String key, Long downTime) {
        this.key = key;
        this.downTime = downTime;
    }
}
